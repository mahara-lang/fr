<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-cpds
 * @author     James Kerrigan
 * @author     Geoffrey Rowland 
 * @author     Dominique-Alain JAN <djan@mac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 James Kerrigan and Geoffrey Rowland geoff.rowland@yeovil.ac.uk
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Formation continue';
$string['description'] = 'Affiche la liste de vos formations continues';
$string['defaulttitledescription'] = 'Si vous laissez cette rubrique vide, le titre de ce bloc sera utilisé.';
$string['newercpds'] = 'Nouvelle formation';
$string['nocpdsaddone'] = 'Votre liste est vide. %sAjoutez une formation continue %s!';
$string['oldercpds'] = 'Précédentes formations continues';
$string['cpdstoshow'] = 'Formations à afficher';
