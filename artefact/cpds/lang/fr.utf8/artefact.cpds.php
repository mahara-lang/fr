<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage artefact-cpds
 * @author     James Kerrigan
 * @author     Geoffrey Rowland 
 * @author     Dominique-Alain JAN <djan@mac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2011 James Kerrigan and Geoffrey Rowland geoff.rowland@yeovil.ac.uk
 *
 */

defined('INTERNAL') || die();

/* cpds */
$string['description'] = 'Description';
$string['deletecpdconfirm'] = 'Voulez-vous réellement supprimer cette formation continue ? La supprimer efface aussi toutes les activités qu\'elle contient.';
$string['deletecpd'] = 'Supprimer la formation continue';
$string['deletethiscpd'] = 'Supprimer la formation continue : \'%s\'';
$string['editcpd'] = 'Modifier la formation continue';
$string['editingcpd'] = 'Modififcation de la formation continue';
$string['manageactivities'] = 'Gestion des activités';
$string['mycpds'] = 'Formations continues';
$string['newcpd'] = 'Nouvelle formation continue';
$string['nocpdsaddone'] = 'Votre liste est vide. %sAjoutez une formation continue%s !';
$string['nocpds'] = 'Aucune formation continue à afficher';
$string['cpd'] = 'Formation continue';
$string['cpds'] = 'Formations continues';
$string['cpddeletedsuccessfully'] = 'Formation continue supprimée avec succès.';
$string['cpdnotdeletedsuccessfully'] = 'Une erreur s\'est produite lors de la suppression de la formation continue.';
$string['cpdnotsavedsuccessfully'] = 'Une erreur s\'est produite lors de l\'envoi de ce formulaire. Modifiez les rubriques en rouge, et essayez à nouveau.';
$string['cpdsavedsuccessfully'] = 'Formation continue enregistrée avec succès.';
$string['cpdsactivities'] = 'Activités de la formation « %s »';
$string['cpdsactivitiesdesc'] = 'Ajoutez des activités à la liste ci-dessous ou utilisez le bouton à droite pour commencer à construire vos activités de formation continue.';
$string['savecpd'] = 'Enregistrer la formation';
$string['title'] = 'Titre';
$string['titledesc'] = 'Le titre sera utilisé pour l\'affichage du bloc dans une Page, en haut de la liste des activités de cette formation.';

/* activities */
$string['at'] = 'à';
$string['allactivities'] = 'Toutes les activités';
$string['hours'] = 'Heures';
$string['totalhours'] = 'Total des heures';
$string['startdate'] = 'Date début';
$string['enddate'] = 'Date fin';
$string['hoursdesc'] = 'Durée en heures consacrées à cette activité (affichage à une décimale, ex 2.5 correspond à 2h30).';
$string['deleteactivityconfirm'] = 'Voulez-vous réellement supprimer cette activité ?';
$string['deleteactivity'] = 'Supprimer cette activité';
$string['deletethisactivity'] = 'Supprimer l\'activité : \'%s\'';
$string['editactivity'] = 'Modifier l\'activité';
$string['editingactivity'] = 'Modification de l\'activité';
$string['myactivities'] = 'Mes activités';
$string['newactivity'] = 'Nouvelle activité';
$string['noactivities'] = 'Aucune activité à afficher.';
$string['noactivitiesaddone'] = 'Aucune activité pour l\'instant. %sAjoutez-en une%s !';
$string['saveactivity'] = 'Enregistrer l\'activité';
$string['activity'] = 'Activité';
$string['activities'] = 'Activités';
$string['activitydeletedsuccessfully'] = 'activité supprimée avec succès.';
$string['activitiesavedsuccessfully'] = 'activité enregistrée avec succès.';
$string['location'] = 'Lieu';
$string['locationdesc'] = 'Le lieu où s\'est déroulé votre activité pour cette formation continue.';
