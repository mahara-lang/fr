// FR lang variables

tinyMCE.addToLang('devkit',{
title : 'Kit de développement TinyMCE',
info_tab : 'Info',
settings_tab : 'Réglages',
log_tab : 'Log',
content_tab : 'Contenu',
command_states_tab : 'Commandes',
undo_redo_tab : 'Annuler/Rétablir',
misc_tab : 'Divers',
filter : 'Filtre:',
clear_log : 'Vider log',
refresh : 'Actualiser',
info_help : 'Presser Actualiser pour voir l\'info.',
settings_help : 'Presser Actualiser pour afficher le tableau des réglages de chaque instance TinyMCE_Control.',
content_help : 'Presser Actualiser pour afficher le contenu HTML brut et nettoyé de chaque instance TinyMCE_Control.',
command_states_help : 'Presser Actualiser pour afficher l\'état de la commande actuelle de inst.queryCommandState. Cette liste indiquera aussi les commandes non supportées.',
undo_redo_help : 'Presser Actualiser pour afficher les niveaux d\'Annuler/Rétablir global et de l\'instance.',
misc_help : 'Voici divers outils de débogage et de développement.',
debug_events : 'Événements de débogage',
undo_diff : 'Diff des niveaux d\'annulation'
});
