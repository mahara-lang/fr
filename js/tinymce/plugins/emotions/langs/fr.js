// FR lang variables

tinyMCE.addToLang('emotions',{
title : 'Insérer binette',
desc : 'Binettes',
cool : 'Cool',
cry : 'Triste',
embarassed : 'Gêné',
foot_in_mouth : 'Pieds dans le plat',
frown : 'Grimace',
innocent : 'Innocent',
kiss : 'Baiser',
laughing : 'Rire',
money_mouth : 'Money mouth',
sealed : 'Bouche cousue',
smile : 'Sourire',
surprised : 'Surprise',
tongue_out : 'Langue tirée',
undecided : 'Indécis',
wink : 'Clin d\'oeil',
yell : 'Cri'
});
