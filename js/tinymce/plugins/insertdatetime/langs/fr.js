// FR lang variables

tinyMCE.addToLang('',{
insertdate_def_fmt : '%%d.%%m.%%y',
inserttime_def_fmt : '%H:%M:%S',
insertdate_desc : 'Insérer date',
inserttime_desc : 'Insérer heure',
inserttime_months_long : new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"),
inserttime_months_short : new Array("Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sep", "Oct", "Nov", "Déc"),
inserttime_day_long : new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"),
inserttime_day_short : new Array("Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim")
});
