// FR lang variables

tinyMCE.addToLang('layer',{
insertlayer_desc : 'Insérer nouveau calque',
forward_desc : 'Déplacer devant',
backward_desc : 'Déplacer derrière',
absolute_desc : 'Basculer vers le positionnement absolu',
content : 'Nouveau calque...'
});
