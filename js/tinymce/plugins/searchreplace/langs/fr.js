// FR lang variables

tinyMCE.addToLang('',{
searchreplace_search_desc : 'Recherche',
searchreplace_searchnext_desc : 'Répétition recherche',
searchreplace_replace_desc : 'Recherche/Remplacement',
searchreplace_notfound : 'La recherche est terminée. Le texte recherché n\'a pas été trouvé.',
searchreplace_search_title : 'Rechercher',
searchreplace_replace_title : 'Rechercher/Remplacer',
searchreplace_allreplaced : 'Toutes les occurrences du texte recherché ont été remplacées.',
searchreplace_findwhat : 'Rechercher',
searchreplace_replacewith : 'Remplacer avec',
searchreplace_direction : 'Direction',
searchreplace_up : 'Vers le haut',
searchreplace_down : 'Vers le bas',
searchreplace_case : 'Respecter la casse',
searchreplace_findnext : 'Suivant',
searchreplace_replace : 'Remplacer',
searchreplace_replaceall : 'Tout&nbsp;remplacer',
searchreplace_cancel : 'Annuler'
});
