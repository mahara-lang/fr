<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/fr.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2011 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['addblog'] = 'Ajouter un blog';
$string['addone'] = 'Ajoutez-en un!';
$string['addpost'] = 'Ajouter un article';
$string['alignment'] = 'Alignement';
$string['allowcommentsonpost'] = 'Autoriser les commentaires sur votre article.';
$string['allposts'] = 'Tous les articles';
$string['alt'] = 'Description';
$string['attach'] = 'Joindre';
$string['attachedfilelistloaded'] = 'Liste des fichiers joints chargée';
$string['attachedfiles'] = 'Fichiers joints';
$string['attachment'] = 'Annexe';
$string['attachments'] = 'Annexes';
$string['baseline'] = 'Ligne de base';
$string['blog'] = 'Blog';
$string['blogcopiedfromanotherview'] = 'Remarque ! Ce bloc a été copié depuis une autre exposition. Vous pouvez le déplacer ou le supprimer, mais ne pouvez pas modifier ce que %s y figure.';
$string['blogdeleted'] = 'Blog supprimé';
$string['blogdesc'] = 'Description';
$string['blogdescdesc'] = 'p.ex., « Expériences et réflexions d\'Héloïse ».';
$string['blogdoesnotexist'] = 'Vous tentez d\'accéder à un blog qui n\'existe pas';
$string['blogpost'] = 'Article de blog';
$string['blogpostdeleted'] = 'Article de blog supprimé';
$string['blogpostdoesnotexist'] = 'Vous tentez d\'accéder à un article de blog qui n\'existe pas';
$string['blogpostpublished'] = 'Article de blog publié';
$string['blogpostsaved'] = 'Article de blog enregistré';
$string['blogs'] = 'Blogs';
$string['blogsettings'] = 'Réglages de blog';
$string['blogtitle'] = 'Titre';
$string['blogtitledesc'] = 'p.ex., « Journal de stage d\'Augustin ».';
$string['border'] = 'Bordure';
$string['bottom'] = 'Bas';
$string['cancel'] = 'Annuler';
$string['cannotdeleteblogpost'] = 'Une erreur est survenue lors de la suppression de cet article de blog.';
$string['copyfull'] = 'Les autres obtiendront leur propre copie de votre %s';
$string['copynocopy'] = 'Sauter ce bloc lors de la copie de l\'exposition';
$string['copyreference'] = 'Les autres pourront afficher votre %s dans leur exposition';
$string['createandpublishdesc'] = 'Ceci créera l\'article de blog et le rendra disponible pour les autres.';
$string['createasdraftdesc'] = 'Ceci créera l\'article de blog, mais il ne sera disponible pour les autres jusqu\'à ce que vous choisissiez de le publier.';
$string['createblog'] = 'Créer un blog';
$string['dataimportedfrom'] = 'Données importées depuis %s';
$string['defaultblogtitle'] = 'Blog de %s';
$string['delete'] = 'Supprimer';
$string['deleteblog?'] = 'Voulez-vous vraiment supprimer ce blog ?';
$string['deleteblogpost?'] = 'Voulez-vous vraiment supprimer cet article de blog ?';
$string['description'] = 'Description';
$string['dimensions'] = 'Dimensions';
$string['draft'] = 'Brouillon';
$string['edit'] = 'Modifier';
$string['editblogpost'] = 'Modifier cet article de blog';
$string['enablemultipleblogstext'] = 'Vous avez un blog. Si vous désirez en créer un autre, activez l\'option de « Permettre plusieurs blogs » depuis la page <a href="%saccount/">Préférences</a> du compte.';
$string['entriesimportedfromleapexport'] = 'Éléments importés à partir d\'une exportation LEAP et n\'ayant pas pu être importés ailleurs';
$string['errorsavingattachments'] = 'Une erreur est survenue lors de l\'enregistrement des fichiers joints à l\'article de blog';
$string['feedrights'] = 'Copyright %s.';
$string['feedsnotavailable'] = 'Les flux RSS ne sont pas disponibles pour ce type d\'artefact';
$string['horizontalspace'] = 'Espace horizontal';
$string['image_list'] = 'Image jointe';
$string['insert'] = 'Insérer';
$string['insertimage'] = 'Insérer une image';
$string['left'] = 'Gauche';
$string['middle'] = 'Milieu';
$string['moreoptions'] = 'Plus d\'options';
$string['mustspecifycontent'] = 'Veuillez écrire du texte pour votre article';
$string['mustspecifytitle'] = 'Veuillez indiquer un titre pour votre article';
$string['myblog'] = 'Mon blog';
$string['myblogs'] = 'Mes blogs';
$string['name'] = 'Nom';
$string['newattachmentsexceedquota'] = 'La taille totale des nouveaux fichiers que vous désirez déposer avec cet article excède votre quota. Vous pourrez enregistrer l\'article si vous retirez un ou plusieurs fichiers que vous avez ajoutés.';
$string['newblog'] = 'Nouveau blog';
$string['newblogpost'] = 'Nouvel article de blog dans le blog «%s»';
$string['newerposts'] = 'Articles plus récents';
$string['nofilesattachedtothispost'] = 'Aucun fichier joint';
$string['noimageshavebeenattachedtothispost'] = 'Aucune image n\'est jointe à cet article. Veuillez déposer ou joindre une image à l\'article avant de pouvoir l\'insérer.';
$string['nopostsyet'] = 'Pas encore d\'article.';
$string['noresults'] = 'Aucun article de blog trouvé';
$string['olderposts'] = 'Articles plus anciens';
$string['pluginname'] = 'Blogs';
$string['post'] = 'article';
$string['postbody'] = 'Corps';
$string['postedbyon'] = 'Écrit par %s le %s';
$string['postedon'] = 'Écrit le';
$string['posts'] = 'articles';
$string['postscopiedfromview'] = 'Articles copiés depuis %s';
$string['posttitle'] = 'Titre';
$string['publish'] = 'Publier';
$string['publishblogpost?'] = 'Voulez-vous vraiment publier cet article ?';
$string['published'] = 'Publié';
$string['publishfailed'] = 'Une erreur est survenue. Votre article n\'est pas publié';
$string['remove'] = 'Supprimer';
$string['right'] = 'Droite';
$string['save'] = 'Enregistrer';
$string['saveandpublish'] = 'Enregistrer et publier';
$string['saveasdraft'] = 'Enregistrer comme brouillon';
$string['savepost'] = 'Enregistrer l\'article';
$string['savesettings'] = 'Enregistrer les réglages';
$string['settings'] = 'Réglages';
$string['src'] = 'URL de l\'image';
$string['textbottom'] = 'Bas du texte';
$string['texttop'] = 'Haut du texte';
$string['thisisdraft'] = 'Cet article est un brouillon';
$string['thisisdraftdesc'] = 'Les brouillons d\'article ne peuvent être vus que par vous.';
$string['title'] = 'Titre';
$string['top'] = 'Haut';
$string['update'] = 'Modifier';
$string['verticalspace'] = 'Espace vertical';
$string['viewblog'] = 'Voir le blog';
$string['viewposts'] = 'Articles copiés (%s)';
$string['youarenottheownerofthisblog'] = 'Vous ne possédez pas ce blog';
$string['youarenottheownerofthisblogpost'] = 'Vous ne possédez pas cet article blog';
$string['youhaveblogs'] = 'Vous avez %s blogs.';
$string['youhavenoblogs'] = 'Vous n\'avez pas de blog.';
$string['youhaveoneblog'] = 'Vous avez 1 blog.';
?>
