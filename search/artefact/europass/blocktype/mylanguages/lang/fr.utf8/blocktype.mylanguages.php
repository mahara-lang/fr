<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2010 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/fr.utf8
 * @author     Gregor Anželj
 * @author     Dominique-Alain Jan <djan@mac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2009-2010 Gregor Anzelj, gregor.anzelj@gmail.com
 *
 */

defined('INTERNAL') || die();

$string['title'] = 'Mes langues';
$string['description'] = 'Afficher vos compétences langagières';

$string['fieldtoshow'] = 'Rubrique à afficher';
$string['filloutyoureuropass'] = '%sRemplissez votre portfolio de langues Europass%s afin d\'y ajouter d\'autres languges !';
$string['displaycompacttable'] = 'Afficher la table en mode compacte';
$string['legend'] = 'Légende';

$string['defaulttitledescription'] = 'Si vous laissez cette rubrique vide, le nom de la rubrique sera utilisé comme titre.';

?>
