<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2009 Catalyst IT Ltd and others; see:
 *                         http://wiki.mahara.org/Contributors
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/fr.utf8
 * @author     Catalyst IT Ltd
 * @author     Nicolas Martignoni <nicolas@martignoni.net>
 * @author     Dominique-Alain Jan <djan@mac.com>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @copyright  (C) 2006-2009 Catalyst IT Ltd http://catalyst.net.nz
 *
 */


defined('INTERNAL') || die();

$string['internal'] = 'Interne';
$string['title'] = 'Interne';
$string['description'] = 'Utiliser la base de données de Mahara comme source d\'authentification';

$string['completeregistration'] = 'Terminer l\'enregistrement';
$string['emailalreadytaken'] = 'Cette adresse de courriel est déjà enregistrée ici';
$string['iagreetothetermsandconditions'] = 'J\'accepte les conditions d\'utilisation';
$string['passwordformdescription'] = 'Votre mot de passe doit comporter au moins six caractères et contenir au moins un chiffre et deux lettres';
$string['passwordinvalidform'] = 'Votre mot de passe doit comporter au moins six caractères et contenir au moins un chiffre et deux lettres';
$string['registeredemailsubject'] = 'Vous vous êtes enregistré sur %s';
$string['registeredemailmessagetext'] = 'Bonjour %s,

Merci de vous être enregistré sur %s. Veuillez suivre le lien ci-dessous pour terminer le processus d\'enregistrement.

%sregister.php?key=%s

Le lien sera échu dans 24 heures.

L\'équipe de %s';
$string['registeredemailmessagehtml'] = '<p>Bonjour %s,</p>
<p>Merci de vous être enregistré sur %s. Veuillez suivre le lien ci-dessous pour terminer le processus d\'enregistrement.</p>
<p><a href="%sregister.php?key=%s">%sregister.php?key=%s</a></p>
<p>Le lien sera échu dans 24 heures.</p>
<p>L\'équipe de %s</p>';
$string['registeredok'] = '<p>Votre enregistrement a réussi. Veuillez consulter votre boîte à lettres pour des instructions sur l\'activation de votre compte.</p>';
$string['registrationnosuchkey'] = 'Désolé, cette clef ne correspond à aucun enregistrement. Peut-être avez-vous attendu plus de 24 heures avant de terminer l\'enregistrement ? Si ce n\'est pas le cas, il s\'agit d\'un problème chez nous.';
$string['registrationunsuccessful'] = 'Désolé, votre enregistrement a échoué. Ce n\'est pas votre faute, mais la nôtre. Veuillez essayer plus tard.';
$string['usernamealreadytaken'] = 'Désolé, ce nom d\'utilisateur est déjà pris';
$string['usernameinvalidform'] = 'Votre nom d\'utilisateur ne peut comporter que des caractères alphanumériques, des points, des soulignés et des arobases. Il doit en outre comporter entre 3 et 30 caractères.';
$string['youmaynotregisterwithouttandc'] = 'Vous ne pouvez pas vous enregistrer sans accepter les <a href="terms.php">conditions d\'utilisation</a>';
$string['youmustagreetothetermsandconditions'] = 'Vous devez accepter les <a href="terms.php">conditions d\'utilisation</a>';
?>
