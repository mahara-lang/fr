// UK lang variables

tinyMCE.addToLang('advimage',{
tab_general : 'Général',
tab_appearance : 'Apparence',
tab_advanced : 'Avancé',
general : 'Général',
title : 'Titre',
preview : 'Prévisualisation',
constrain_proportions : 'Conserver les proportions',
langdir : 'Direction de l\'écriture',
langcode : 'Code de langue',
long_desc : 'Lien vers la description',
style : 'Style',
classes : 'Classes',
ltr : 'Gauche à droite',
rtl : 'Droite à gauche',
id : 'Id',
image_map : 'Carte image',
swap_image : 'Échanger image',
alt_image : 'Image alternative',
mouseover : 'for mouse over',
mouseout : 'for mouse out',
misc : 'Divers',
example_img : 'Apparence&nbsp;prévisualisation&nbsp;image',
missing_alt : 'Voulez-vous vraiment continuer sans inclure une description&nbsp;? Il est possible que sans cette description l\'image ne soit pas accessible pour des utilisateurs handicapés ou ceux utilisant un navigateur texte ou consultant le web sans les images.'
});
