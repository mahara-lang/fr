// FR lang variables

tinyMCE.addToLang('',{
paste_text_desc : 'Copier sans formatage',
paste_text_title : 'Utiliser CTRL+V sur votre clavier pour coller le texte dans la fenêtre.',
paste_text_linebreaks : 'Conserver les sauts de ligne',
paste_word_desc : 'Copier depuis Word',
paste_word_title : 'Utiliser CTRL+V sur votre clavier pour coller le texte dans la fenêtre.',
selectall_desc : 'Tout sélectionner'
});
