// FR lang variables

tinyMCE.addToLang('template',{
title : 'Modèles',
label : 'Modèle',
desc_label : 'Description',
desc : 'Insérer du contenu dans un modèle prédéfini',
select : 'Choisir un modèle',
preview : 'Prévisualisation',
warning : 'Attention&nbsp;! Passer d\'un modèle à un autre risque de causer une perte de données.',
def_date_format : '%d.%m.%Y %H:%M:%S',
months_long : new Array("Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Août", "Septembre", "Octobre", "Novembre", "Décembre"),
months_short : new Array("Jan", "Fév", "Mar", "Avr", "Mai", "Juin", "Juil", "Août", "Sept", "Oct", "Nov", "Déc"),
day_long : new Array("Dimanche", "Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi", "Dimanche"),
day_short : new Array("Dim", "Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim")
});
