<?php

defined('INTERNAL') || die();

$string['element.bytes.bytes'] = 'Octets';
$string['element.bytes.kilobytes'] = 'Kilooctets';
$string['element.bytes.megabytes'] = 'Mégaoctets';
$string['element.bytes.gigabytes'] = 'Gigaoctets';
$string['element.bytes.invalidvalue'] = 'Valeur doit être un nombre';

$string['element.calendar.invalidvalue'] = 'Date/Heure indiquée invalide';

$string['element.date.or'] = 'ou';
$string['element.date.monthnames'] = 'janvier,février,mars,avril,mai,juin,juillet,août,septembre,octobre,novembre,décembre';
$string['element.date.notspecified'] = 'Non indiqué';

$string['element.expiry.days'] = 'Jours';
$string['element.expiry.weeks'] = 'Semaines';
$string['element.expiry.months'] = 'Mois';
$string['element.expiry.years'] = 'Années';
$string['element.expiry.noenddate'] = 'Pas de date de fin';

$string['rule.before.before'] = 'Ceci ne peut pas être placé après le champ "%s"';

$string['rule.email.email'] = 'Cette adresse de courriel n\'est pas valide';

$string['rule.integer.integer'] = 'Ce champ doit être un nombre entier';

$string['rule.maxlength.maxlength'] = 'Ce champ ne peut pas contenir plus de %d signes';

$string['rule.minlength.minlength'] = 'Ce champ doit contenir au moins %d caractères';

$string['rule.minvalue.minvalue'] = 'Cette valeur ne peut pas être inférieure à %d';

$string['rule.regex.regex'] = 'Ce champ n\'a pas une forme correcte';

$string['rule.required.required'] = 'Ce champ est obligatoire';

$string['rule.validateoptions.validateoptions'] = 'Cette option "%s" n\'est pas valide';

$string['rule.maxvalue.maxvalue'] = 'Cette valeur ne peut pas supérieure à %d';
