<?php
/**
 * Mahara: Electronic portfolio, weblog, resume builder and social networking
 * Copyright (C) 2006-2008 Catalyst IT Ltd (http://www.catalyst.net.nz)
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @package    mahara
 * @subpackage lang/fr.utf8
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL
 * @author     Your Name <your@email.address>
 * @copyright  (C) 2006-2011 Catalyst IT Ltd http://catalyst.net.nz
 *
 */

defined('INTERNAL') || die();

$string['15,70,15'] = 'Colonne centrale beaucoup plus large';
$string['20,30,30,20'] = 'Colonnes centrales plus larges';
$string['25,25,25,25'] = 'Largeurs égales';
$string['25,50,25'] = 'Colonne centrale plus large';
$string['33,33,33'] = 'Largeurs égales';
$string['33,67'] = 'Colonne de droite plus large';
$string['50,50'] = 'Largeurs égales';
$string['67,33'] = 'Colonne de gauche plus large';
$string['Added'] = 'Ajouté';
$string['Browse'] = 'Consulter';
$string['Configure'] = 'Configurer';
$string['Owner'] = 'Possesseur';
$string['Preview'] = 'Preview';
$string['Search'] = 'Rechercher';
$string['Submitted'] = 'Envoyée';
$string['Template'] = 'Gabarit';
$string['Untitled'] = 'Sans titre';
$string['View'] = 'Exposition';
$string['Views'] = 'Expositions';
$string['access'] = 'Accès';
$string['accessbetweendates2'] = 'Personne d\'autre ne peut consulter cette exposition avant %s ou après %s';
$string['accessfromdate2'] = 'Personne d\'autre ne peut consulter cette exposition avant %s';
$string['accessuntildate2'] = 'Personne d\'autre ne peut consulter cette exposition après %s';
$string['add'] = 'Ajouter';
$string['addcolumn'] = 'Ajouter colonne';
$string['addedtowatchlist'] = 'Cette exposition a été ajoutée à votre liste de suivi';
$string['addnewblockhere'] = 'Ajouter un nouveau bloc ici';
$string['addtowatchlist'] = 'Ajouter l\'exposition à la liste de suivi';
$string['addtutors'] = 'Ajouter tuteurs';
$string['addusertogroup'] = 'Ajouter cet utilisateur à un groupe';
$string['allowcommentsonview'] = 'Si sélectionné, les utilisateurs qui peuvent accéder votre exposition pourront laisser des commentaires.';
$string['allowcopying'] = 'Permettre la copie';
$string['allviews'] = 'Toutes les expositions';
$string['alreadyinwatchlist'] = 'Cette exposition est déjà dans votre liste de suivi';
$string['artefacts'] = 'Productions';
$string['artefactsinthisview'] = 'Productions dans cette exposition';
$string['attachedfileaddedtofolder'] = 'Le fichier joint %s a été ajouté à votre dossier « %s ».';
$string['attachment'] = 'Annexe';
$string['back'] = 'Précédent';
$string['backtocreatemyview'] = 'Retour à la création de mon exposition';
$string['backtoyourview'] = 'Retour à votre exposition';
$string['blockconfigurationrenderingerror'] = 'La configuration a échoué car le bloc ne peut pas être affiché correctement.';
$string['blockcopypermission'] = 'Permission de copie du bloc';
$string['blockcopypermissiondesc'] = 'Si vous autorisez d\'autres utilisateurs à copier cette exposition, vous pouvez choisir comment ce bloc sera copié';
$string['blockinstanceconfiguredsuccessfully'] = 'Bloc configuré';
$string['blocksinstructionajax'] = 'Déplacer les blocs au-dessous de cette ligne pour les ajouter à l\'affichage de votre exposition. Vous pouvez déplacer les blocs pour les positionner.';
$string['blocksintructionnoajax'] = 'Sélectionner un bloc et choisir où l\'ajouter dans votre exposition. Vous pouvez positionner un bloc à l\'aide des flèches de sa barre de titre.';
$string['blocktitle'] = 'Titre du bloc';
$string['blocktypecategory.feeds'] = 'Flux externes';
$string['blocktypecategory.fileimagevideo'] = 'Fichiers, images et vidéos';
$string['blocktypecategory.general'] = 'Général';
$string['by'] = 'par';
$string['cantaddcolumn'] = 'Vous ne pouvez pas ajouter de colonne supplémentaire à cette exposition';
$string['cantdeleteview'] = 'Vous ne pouvez pas supprimer cette exposition';
$string['canteditdontown'] = 'Vous ne pouvez pas modifier cette exposition, car elle ne vous appartient pas';
$string['canteditsubmitted'] = 'Vous ne pouvez pas modifier cette exposition, car elle a été envoyée au groupe « %s » pour évaluation. Vous devez attendre qu\'un tuteur libère votre exposition.';
$string['cantremovecolumn'] = 'Vous ne pouvez pas supprimer la dernière colonne à une exposition';
$string['cantsubmitviewtogroup'] = 'Vous ne pouvez pas envoyer cette exposition à ce groupe pour évaluation';
$string['changemyviewlayout'] = 'Modifier l\'affichage de mon exposition';
$string['changeviewlayout'] = 'Modifier les colonnes';
$string['changeviewtheme'] = 'Le thème que vous aviez sélectionné pour cette exposition n\'est plus disponible. Veuillez choisir un autre thème.';
$string['choosetemplategrouppagedescription'] = '<p>Vous pouvez rechercher ici parmi les expositions dont votre Groupe a le droit de copier pour en construire une nouvelle. Une miniature de l\'exposition est affichée si vous cliquez sur son nom. Une fois l\'exposition choisie, cliquez sur le bouton «Copier l\'exposition» correspondant pour en faire une copie et la modifier à votre convenance.</p><p><strong>Attention !</strong> Au niveau du groupe, on ne peut pas faire de copies des blogs ou d\'articles de blog.</p>';
$string['choosetemplateinstitutionpagedescription'] = '<p>Vous pouvez rechercher ici parmi les expositions que votre Institution a le droit de copier pour en construire une nouvelle. Une miniature de l\'exposition est affichée si vous cliquez sur son nom. Une fois l\'exposition choisie, cliquez sur le bouton «Copier l\'exposition» correspondant pour en faire une copie et la modifier à votre convenance.</p><p><strong>Attention !</strong> Au niveau de l\' institution on ne peut pas faire de copies des blogs ou d\'articles de blog.</p>';
$string['choosetemplatepagedescription'] = '<p>Vous pouvez rechercher ici parmi les expositions que vous avez le droit de copier pour en construire une nouvelle. Une miniature de l\'exposition est affichée si vous cliquez sur son nom. Une fois l\'exposition choisie, cliquez sur le bouton «Copier l\'exposition» correspondant pour en faire une copie et la modifier à votre convenance.</p>';
$string['clickformoreinformation'] = 'Cliquer ici pour plus d\'information et placer votre commentaire';
$string['complaint'] = 'Plainte';
$string['configureblock'] = 'Configurer ce bloc';
$string['configurethisblock'] = 'Configurer ce bloc';
$string['confirmcancelcreatingview'] = 'Cette exposition n\'est pas terminée. Voulez-vous vraiment annuler ?';
$string['confirmdeleteblockinstance'] = 'Voulez-vous vraiment supprimer ce bloc ?';
$string['copiedblocksandartefactsfromtemplate'] = '%d bloc(s) et %d production(s) copiés depuis «%s»';
$string['copyaview'] = 'Copier une exposition';
$string['copyfornewgroups'] = 'Copier pour les nouveaux groupes';
$string['copyfornewgroupsdescription'] = 'Faire une copie de cette exposition pour les nouveaux groupes de ces types :';
$string['copyfornewmembers'] = 'Copier pour les nouveaux membres de l\'institution';
$string['copyfornewmembersdescription'] = 'Faire automatiquement une copie de cette exposition pour tous les nouveaux membres de %s';
$string['copyfornewusers'] = 'Copier pour les nouveaux utilisateurs';
$string['copyfornewusersdescription'] = 'Faire automatiquement une copie de cette exposition dans le portfolio de chaque utilisateur nouvellement créé.';
$string['copynewusergroupneedsloggedinaccess'] = 'Les expositions copiées pour les nouveaux utilisateurs ou groupes doivent être accessibles aux utilisateurs connectés.';
$string['copythisview'] = 'Copier cette exposition';
$string['copyview'] = 'Copier l\'exposition';
$string['createemptyview'] = 'Créer une exposition vide';
$string['createview'] = 'Créer une exposition';
$string['dashboard'] = 'Tableau de bord';
$string['dashboardviewtitle'] = 'Tableau de bord';
$string['date'] = 'Date';
$string['deletespecifiedview'] = 'Supprimer l\'exposition « %s »';
$string['deletethisview'] = 'Supprimer cette exposition';
$string['deleteviewconfirm'] = 'Voulez-vous vraiment supprimer cette exposition ? Cette opération ne pourra pas être annulée.';
$string['deleteviewconfirmnote'] = '<p><strong>Note :</strong> les blocs de contenu qui ont été ajoutés à l\'exposition ne seront pas supprimés. Toutefois, les commentaires effectués sur cette exposition seront eux effacés. Pensez à faire une copie de cette exposition avant de procéder à cette suppression.</p>';
$string['description'] = 'Description exposition';
$string['displaymyview'] = 'Afficher mon exposition';
$string['editaccess'] = 'Modifier accès';
$string['editaccessdescription'] = '<p>Par défaut, vous ne pouvez voir que votre %s. Vous pouvez partager votre %s avec d\'autres utilisateurs en ajoutant des règles de partage.</p><p>Quand vous désirez continuer, allez au bas de cette page et cliquez sur Enregistrer.</p>';
$string['editaccesspagedescription2'] = '<p>Par défaut, vous ne pouvez consulter que votre propre exposition. Vous pouvez choisir ici à qui donner accès à votre exposition.</p>
<p>Lorsque votre sélection est faite, faites défiler la fenêtre vers le bas et cliquer «Enregistrer» pour continuer.</p>';
$string['editblockspagedescription'] = '<p>Sélectionnez dans les onglets ci-dessous les blocs que vous voulez ajouter. Glisser-déposer ensuite les blocs de contenu dans votre exposition.</p>';
$string['editcontent'] = 'Modifier contenu';
$string['editcontentandlayout'] = 'Modifier contenu et présentation';
$string['editmyview'] = 'Modifier mon exposition';
$string['editprofileview'] = 'Modifier l\'exposition du profil';
$string['editthisview'] = 'Modifier cette exposition';
$string['edittitle'] = 'Modifier titre';
$string['edittitleanddescription'] = 'Modifier titre et description';
$string['empty_block'] = 'Sélectionner dans l\'arborescence à gauche une production à placer ici';
$string['emptylabel'] = 'Cliquez ici pour saisir un texte pour cette étiquette';
$string['err.addblocktype'] = 'Impossible d\'ajouter ce bloc à votre exposition';
$string['err.addcolumn'] = 'Impossible d\'ajouter la nouvelle colonne';
$string['err.changetheme'] = 'Impossible de modifier le thème';
$string['err.moveblockinstance'] = 'Impossible de déplacer le bloc à l\'emplacement indiqué';
$string['err.removeblockinstance'] = 'Impossible de supprimer le bloc';
$string['err.removecolumn'] = 'Impossible de supprimer la colonne';
$string['everyoneingroup'] = 'Tout le monde dans le groupe';
$string['filescopiedfromviewtemplate'] = 'Fichiers copiés depuis %s';
$string['forassessment'] = 'pour évaluation';
$string['friend'] = 'Ami';
$string['friends'] = 'Amis';
$string['friendslower'] = 'amis';
$string['grouphomepage'] = 'Page d\'accueil du groupe';
$string['grouphomepagedescription'] = 'L\'exposition de la page d\'accueil du groupe est celle qui sera affichée dans l\'onglet «A propos» pour ce groupe';
$string['grouphomepageviewtitle'] = 'Page d\'accueil du groupe';
$string['grouplower'] = 'groupe';
$string['groups'] = 'Groupes';
$string['groupviews'] = 'Expositions du groupe';
$string['in'] = 'dans';
$string['institutionviews'] = 'Expositions de l\'institution';
$string['invalidcolumn'] = 'La colonne %s est invalide';
$string['inviteusertojoingroup'] = 'Inviter cet utilisateur à joindre un groupe';
$string['listviews'] = 'Liste des expositions';
$string['loggedin'] = 'Utilisateurs connectés';
$string['loggedinlower'] = 'utilisateurs connectés';
$string['moveblockdown'] = 'Déplacer le bloc %s vers le bas';
$string['moveblockleft'] = 'Déplacer le bloc %s à gauche';
$string['moveblockright'] = 'Déplacer le bloc %s à droite';
$string['moveblockup'] = 'Déplacer le bloc %s vers le haut';
$string['movethisblockdown'] = 'Déplacer ce bloc vers le bas';
$string['movethisblockleft'] = 'Déplacer ce bloc à gauche';
$string['movethisblockright'] = 'Déplacer ce bloc à droite';
$string['movethisblockup'] = 'Déplacer ce bloc vers le haut';
$string['myviews'] = 'Mes expositions';
$string['next'] = 'Suivant';
$string['noaccesstoview'] = 'Vous n\'êtes pas autorisé à accéder à cette exposition';
$string['noartefactstochoosefrom'] = 'Désolé, aucune production disponible dans';
$string['noblocks'] = 'Désolé, pas de bloc dans cette catégorie :(';
$string['nobodycanseethisview2'] = 'Vous seul pouvez voir cette exposition';
$string['nocopyableviewsfound'] = 'Aucune exposition à copier';
$string['noownersfound'] = 'Aucun possesseur trouvé';
$string['notifysiteadministrator'] = 'Informer l\'administrateur du site';
$string['notitle'] = 'Sans titre';
$string['notobjectionable'] = 'Contenu correct';
$string['noviewlayouts'] = 'Il n\'a y pas d\'affichage prévu pour une exposition à %s colonnes';
$string['noviews'] = 'Pas d\'exposition';
$string['numberofcolumns'] = 'Nombre de colonnes';
$string['overridingstartstopdate'] = 'Imposer d\'autres dates de début/fin';
$string['overridingstartstopdatesdescription'] = 'Vous pouvez, si vous le voulez, fixer d\'autres dates de début et/ou de fin. D\'autres personnes ne pourront pas voir votre exposition avant la date de début et après la date de fin, en plus des autres accès que vous avez accordés.';
$string['owner'] = 'possesseur';
$string['ownerformat'] = 'Format d\'affichage du nom';
$string['ownerformatdescription'] = 'Comment voulez-vous que les personnes consultant votre exposition voient votre nom ?';
$string['owners'] = 'possesseurs';
$string['peoplewiththesecreturl'] = 'Les personnes qui connaissent l\'URL secrète';
$string['portfolio'] = 'Portfolio';
$string['print'] = 'Imprimer';
$string['profile'] = 'Profile';
$string['profileicon'] = 'Icône du profil';
$string['profileviewtitle'] = 'Exposition du profil';
$string['public'] = 'Public';
$string['publiclower'] = 'public';
$string['reallyaddaccesstoemptyview'] = 'Votre exposition ne contient pas de bloc. Voulez-vous vraiment y donner accès à ces utilisateurs ?';
$string['remove'] = 'Supprimer';
$string['removeblock'] = 'Supprimer ce bloc';
$string['removecolumn'] = 'Supprimer cette colonne';
$string['removedfromwatchlist'] = 'Cette exposition a été retirée de votre liste de suivi';
$string['removefromwatchlist'] = 'Retirer l\'exposition de la liste de suivi';
$string['removethisblock'] = 'Retirer ce bloc';
$string['reportobjectionablematerial'] = 'Annoncer du matériel discutable';
$string['reportsent'] = 'Votre annonce a été envoyée';
$string['searchowners'] = 'Rechercher les possesseurs';
$string['searchviews'] = 'Rechercher les expositions';
$string['searchviewsbyowner'] = 'Rechercher les expositions par possesseur :';
$string['selectaviewtocopy'] = 'Sélectionner l\'exposition que vous voulez copier :';
$string['show'] = 'Afficher';
$string['startdate'] = 'Date/heure début d\'accès';
$string['startdatemustbebeforestopdate'] = 'La date de début doit précéder la date de fin';
$string['stopdate'] = 'Date/heure fin d\'accès';
$string['stopdatecannotbeinpast'] = 'La date de fin de ne peut pas être dans le passé.';
$string['submitaviewtogroup'] = 'Envoyer une exposition à ce groupe pour évaluation';
$string['submittedforassessment'] = 'Envoyée pour évaluation';
$string['submitthisviewto'] = 'Envoyer cette exposition à';
$string['submitviewconfirm'] = 'Si vous envoyez « %s » à « %s » pour évaluation, vous ne pourrez plus la modifier jusqu\'à ce que votre tuteur ait terminé l\'évaluation de l\'exposition. Voulez-vous vraiment envoyer cette exposition maintenant ?';
$string['submitviewtogroup'] = 'Envoyer « %s » à « %s » pour évaluation';
$string['success.addblocktype'] = 'Bloc ajouté';
$string['success.addcolumn'] = 'Colonne ajoutée';
$string['success.changetheme'] = 'Thème modifié';
$string['success.moveblockinstance'] = 'Bloc déplacé';
$string['success.removeblockinstance'] = 'Bloc supprimé';
$string['success.removecolumn'] = 'Colonne supprimée';
$string['templatedescription'] = 'Cochez cette case si vous voulez que les personnes consultant votre exposition puissent en faire en faire une copie.';
$string['templatedescriptionplural'] = 'Cochez cette case si vous souhaitez que les personnes qui peuvent accéder à vos expositions puissent aussi en faire des copies, comprenant tous les fichiers et les dossiers qu\'elles contiennent.';
$string['thisviewmaybecopied'] = 'La copie est autorisée';
$string['timeofsubmission'] = 'Heure de l\'envoi';
$string['title'] = 'Titre de l\'exposition';
$string['token'] = 'URL secrète';
$string['tutors'] = 'tuteurs';
$string['unrecogniseddateformat'] = 'Format de date non reconnu';
$string['updatewatchlistfailed'] = 'Échec de la modification de la liste de suivi';
$string['users'] = 'Utilisateurs';
$string['view'] = 'exposition';
$string['viewaccesseditedsuccessfully'] = 'Accès à l\'exposition enregistré';
$string['viewcolumnspagedescription'] = 'Veuillez d\'abord choisir le nombre de colonnes de votre exposition. À l\'étape suivante, vous pourrez modifier la largeur des colonnes.';
$string['viewcopywouldexceedquota'] = 'La copie de cette exposition dépasserait votre quota de fichiers.';
$string['viewcreatedsuccessfully'] = 'Exposition créée';
$string['viewdeleted'] = 'Exposition supprimée';
$string['viewfilesdirdesc'] = 'Fichier d\'expositions copiées';
$string['viewfilesdirname'] = 'viewfiles';
$string['viewinformationsaved'] = 'Information de l\'exposition enregistrée';
$string['viewlayoutchanged'] = 'Disposition des colonnes de l\'exposition modifiée';
$string['viewlayoutpagedescription'] = 'Choisissez la disposition des colonnes de votre exposition';
$string['viewobjectionableunmark'] = 'Cette exposition, ou un de ses éléments, a été rapporté comme contenant du contenu non désirable. Si cela n\'est plus cas, vous pouvez cliquer sur le bouton ci-dessous pour supprimer cette note et en avertir les autres administrateurs.';
$string['views'] = 'expositions';
$string['viewsavedsuccessfully'] = 'Exposition enregistrée';
$string['viewsby'] = 'Expositions de %s';
$string['viewscopiedfornewgroupsmustbecopyable'] = 'Vous devez autoriser la copie avant de définir qu\'une exposition soit copiable pour les nouveaux utilisateurs.';
$string['viewscopiedfornewusersmustbecopyable'] = 'Vous devez autoriser la copie avant de définir qu\'une exposition soit copiable pour les nouveaux groupes.';
$string['viewsownedbygroup'] = 'Expositions appartenant à ce groupe';
$string['viewssharedtogroup'] = 'Expositions partagées pour ce groupe';
$string['viewssharedtogroupbyothers'] = 'Expositions partagées pour ce groupe par d\'autres';
$string['viewssubmittedtogroup'] = 'Expositions envoyées à ce groupe';
$string['viewsubmitted'] = 'Exposition envoyée à l\'évaluation';
$string['viewsubmittedtogroup'] = 'Cette exposition a été envoyée à <a href="%s">%s</a>';
$string['viewsubmittedtogroupon'] = 'Cette exposition a été envoyée à <a href="%s">%s</a>  dans %s';
$string['viewtitleby'] = '%s par <a href="%s">%s</a>';
$string['viewunobjectionablebody'] = '%s a consulté %s publiée par %s et l\'a marqué comme ne comportant plus de contenu non désirable.';
$string['viewunobjectionablesubject'] = 'L\'exposition %s était marquée, par %s, comme comportant du contenu non désirable';
$string['viewvisitcount'] = '%d visite(s) de %s à %s';
$string['watchlistupdated'] = 'Votre liste de suivi a été modifiée';
$string['whocanseethisview'] = 'Exposition consultable par';
$string['youhavenoviews'] = 'Vous n\'avez pas d\'exposition';
$string['youhaveoneview'] = 'Vous avez 1 exposition.';
$string['youhavesubmitted'] = 'Vous avez envoyé <a href="%s">%s</a> pour évaluation à ce groupe';
$string['youhavesubmittedon'] = 'Vous avez envoyé <a href="%s">%s</a> pour évaluation à ce groupe dans %s';
$string['youhaveviews'] = 'Vous avez %s expositions.';
?>
